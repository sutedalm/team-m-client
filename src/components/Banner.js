import "../styles/Banner.css";

import React from "react";
import { Col, Container, Jumbotron, Row } from "react-bootstrap";

const Banner = () => {
  return (
    <div className="Banner">
      <h1>Kidney Stone of Truth</h1>
      <p>Fact check controversial statements about Kidney Stones</p>
    </div>
  );
};

export default Banner;
