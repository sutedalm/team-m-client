import "../styles/SpinningStone.css";

import React from "react";
import stone from "../images/stone.svg";

const SpinningStone = () => {
  return (
    <div className="SpinningStone">
      <img
        className="SpinningStone__Icon"
        src={stone}
        alt="Please wait..."
        width={100}
        height={100}
      />
    </div>
  );
};

export default SpinningStone;
