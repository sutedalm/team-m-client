import React from "react";
import { Card, Col, Row } from "react-bootstrap";
import { colorDict, textColorDict } from "../constants";

const ResultChart = ({ controversialityText, confidenceScore }) => {
  confidenceScore = Math.round(confidenceScore * 100);

  const controversialityPillStyle = {
    backgroundColor: colorDict[controversialityText],
    color: textColorDict[controversialityText],
  };

  const confidenceBarStyle = {
    width: `${confidenceScore}%`,
    backgroundColor: "lightgrey",
  };

  return (
    <div className="mt-4 mb-5">
      <Card>
        <Card.Body>
          <Row className="align-items-center">
            <Col xs={12} md={3}>
              <span>Result:</span>
            </Col>
            <Col xs={12} md={9}>
              <Card>
                <Card.Body
                  className="py-1 text-center rounded"
                  style={controversialityPillStyle}
                >
                  {controversialityText}
                </Card.Body>
              </Card>
            </Col>
          </Row>

          <Row className="align-items-center mt-2">
            <Col xs={12} md={3}>
              <span>Confidence Score:</span>
            </Col>
            <Col xs={12} md={9}>
              <div className="bg-light rounded px-0 overflow-hidden">
                <div
                  className="h-100 py-1 text-center text-white"
                  style={confidenceBarStyle}
                >
                  {confidenceScore}%
                </div>
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </div>
  );
};

ResultChart.defaultProps = {
  controversialityText: "CONTRADICTION",
  confidenceScore: "69",
};

export default ResultChart;
