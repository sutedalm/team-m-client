import "../styles/MainForm.css";

import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";

const MainForm = ({ handleSubmit }) => {
  const [requestedSentence, setRequestedSentence] = useState("");

  const handleSubmitWrapper = (e) => {
    handleSubmit(requestedSentence);
    e.preventDefault();
  };

  return (
    <div className="MainForm shadow rounded">
      <Form onSubmit={handleSubmitWrapper}>
        <Form.Group controlId="formTextArea">
          <Form.Label>Type in a controversial Sentence:</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            placeholder="e.g.: Cranberry Juice helps to prevent Kidney Stones"
            value={requestedSentence}
            onChange={(e) => setRequestedSentence(e.target.value)}
            required
          />
        </Form.Group>
        <Button className="w-100 mt-0" type="submit">
          Fact check it!
        </Button>
      </Form>
    </div>
  );
};

export default MainForm;
