import React from "react";
import { Col, Row } from "react-bootstrap";
import { colorDict, textColorDict } from "../constants";

const ControversialityStatsbar = ({
  controversialityText,
  confidenceScore,
}) => {
  const controversialityTextColor = textColorDict[controversialityText];
  const controversialityColor = colorDict[controversialityText];

  const confidenceBarStyle = {
    width: `${confidenceScore}%`,
    background: "lightgrey",
    fontSize: "0.8em",
  };

  const controversialityPillStyle = {
    backgroundColor: controversialityColor,
    color: controversialityTextColor,
    fontSize: "0.8em",
  };

  return (
    <div className="ControversialityStatsbar">
      <Row className="mb-3 statsbar mr-1">
        <Col xs={5}>
          <Col
            xs={12}
            className="rounded text-nowrap py-1 px-0 w-100 h-100 text-center"
            style={controversialityPillStyle}
          >
            {controversialityText}
          </Col>
        </Col>
        <Col xs={7} className="bg-light rounded px-0 overflow-hidden">
          <div
            className="h-100 px-2 py-1 text-center text-nowrap"
            style={confidenceBarStyle}
          >
            {confidenceScore}% Confidence
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default ControversialityStatsbar;
