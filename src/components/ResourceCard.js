import "../styles/ResourceCard.css";

import React from "react";
import { Button, Card, Col, Row } from "react-bootstrap";

import { colorDict, textColorDict } from "../constants";
import ControversialityStatsbar from "./ControversialityStatsbar";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuoteLeft } from "@fortawesome/free-solid-svg-icons";

const ResourceCard = ({
  controversialityText,
  confidenceScore,
  content,
  author,
  title,
  text,
  link,
}) => {
  confidenceScore = Math.round(confidenceScore * 100);

  return (
    <div className="ResourceCard">
      <div className="ResourceCard-inner shadow">
        <div className="ResourceCard-front">
          <ControversialityStatsbar
            controversialityText={controversialityText}
            confidenceScore={confidenceScore}
          />
          <div>
            <FontAwesomeIcon
              icon={faQuoteLeft}
              className="ResourceCard-front__quote-icon"
            />
            <strong>
              <q>{content}</q>
            </strong>
            <p className="ResourceCard-front__author">
              {author && " - "}
              {author}
            </p>
          </div>
          <Button
            variant="primary"
            href={link}
            className="ResourceCard__button stretched-link w-100"
          >
            Read More
          </Button>
        </div>
        <div className="ResourceCard-back">
          <ControversialityStatsbar
            controversialityText={controversialityText}
            confidenceScore={confidenceScore}
          />
          <h4 className="ResourceCard__title">{title}</h4>
          <p className="ResourceCard__body">{text}</p>
          <Button
            variant="primary"
            href={link}
            className="ResourceCard__button stretched-link w-100"
          >
            Read More
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ResourceCard;
