import { gql, useLazyQuery } from "@apollo/client";
import React from "react";
import MainForm from "./MainForm";
import Resources from "./Resources";
import ResultChart from "./ResultChart";
import SpinningStone from "./SpinningStone";

const ANALYZE_SENTENCE_QUERY = gql`
  query AnalyzeSentence($sentence: String!) {
    analyzeSentenceControversiality(sentence: $sentence) {
      result {
        confidenceScore
        controversiality
      }
      resources {
        predictedLabel
        predictedScore
        link
        text
        title
        content
        author
      }
    }
  }
`;

const MainBody = () => {
  const [submitQuery, { loading, data }] = useLazyQuery(ANALYZE_SENTENCE_QUERY);

  const handleSubmit = (requestedSentence) => {
    submitQuery({
      variables: {
        sentence: requestedSentence,
      },
    });
  };

  return (
    <div className="MainBody">
      <MainForm handleSubmit={handleSubmit} />
      {loading && (
        <SpinningStone />
        // <ReactLoading type="bars" color="lightblue" className="mx-auto" />
      )}
      {data && (
        <>
          <ResultChart
            controversialityText={
              data.analyzeSentenceControversiality.result.controversiality
            }
            confidenceScore={
              data.analyzeSentenceControversiality.result.confidenceScore
            }
          />
          <Resources
            resources={data.analyzeSentenceControversiality.resources}
            controversiality={
              data.analyzeSentenceControversiality.result.controversiality
            }
          />
        </>
      )}
    </div>
  );
};

export default MainBody;
