import React from "react";
import { CardDeck, Container, Row } from "react-bootstrap";
import ResourceCard from "./ResourceCard";

function groupBy(list, keyGetter) {
  const map = new Map();
  list.forEach((item) => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
}

const sortResources = (resources, controversiality) => {
  // group resources by their controversiality
  let grouped = groupBy(resources, (resource) => resource.predictedLabel);

  // sort groups internally by their confidence (Descending)
  grouped.forEach((value, key, map) => {
    value.sort((a, b) => b.predictedScore - a.predictedScore);
  });

  let priorities = {
    ENTAILMENT: 5,
    CONTRADICTION: 5,
    CONTROVERSIAL: 7,
    NEUTRAL: 10,
  };
  priorities[controversiality] = 1;

  // sort groups for relevancy
  let result = Array.from(grouped.entries());
  result = result
    .sort((a, b) => priorities[a[0]] - priorities[b[0]])
    .map((i) => i[1]);

  // flatten groups into one array
  result = [].concat.apply([], result);

  return result;
};

const Resources = ({ resources, controversiality }) => {
  resources = sortResources(resources, controversiality);
  return (
    <div>
      <h2 className="text-center w-100 mt-3">Resources</h2>
      <CardDeck className="justify-content-around">
        {resources.map((resource) => (
          <ResourceCard
            title={resource.title}
            content={resource.content}
            author={resource.author}
            text={resource.text}
            link={resource.link}
            confidenceScore={resource.predictedScore}
            controversialityText={resource.predictedLabel}
          />
        ))}
      </CardDeck>
    </div>
  );
};

export default Resources;
