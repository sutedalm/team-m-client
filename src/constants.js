export const colorDict = {
  ENTAILMENT: "lightgreen",
  CONTRADICTION: "indianred",
  CONTROVERSIAL: "mediumpurple",
  NEUTRAL: "lightgrey",
};

export const textColorDict = {
  ENTAILMENT: "black",
  CONTRADICTION: "white",
  CONTROVERSIAL: "white",
  NEUTRAL: "black",
};

// export const serverEndpoint = "http://127.0.0.1:8000/graphql";
export const serverEndpoint = "https://nlp.marc-sinner.de/graphql";

// export const serverEndpoint =
//   "http://ec2-3-125-9-43.eu-central-1.compute.amazonaws.com:8080/graphql";
