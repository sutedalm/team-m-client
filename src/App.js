import { Container } from "react-bootstrap";
import Banner from "./components/Banner";
import MainBody from "./components/MainBody";
import "./styles/App.css";

function App() {
  return (
    <div className="App">
      <Banner />
      <div className="Banner__Separator"></div>
      <Container>
        {/* <div className="kidney-parralax"></div> */}
        <MainBody />
      </Container>
    </div>
  );
}

export default App;
